import 'dart:ui';
import 'package:coin_dude/modules/app/app_bindings.dart';
import 'package:coin_dude/utils/core_initializer.dart';
import 'package:coin_dude/utils/app_translations.dart';
import 'package:coin_dude/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'modules/app/app_page.dart';
import 'modules/ticker_details/ticker_details_bindings.dart';
import 'modules/ticker_details/ticker_details_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  await CoreInitializer.initCore();

  runApp(GetMaterialApp(
    initialRoute: '/app',
    translations: AppTranslations(),
    locale: Locale('en', 'GB'),
    theme: Themes.ligntTheme,
    getPages: [
      GetPage(
        name: '/app',
        page: () => AppPage(),
        binding: AppBindings(),
      ),
      GetPage(
        name: '/ticker_details',
        page: () => TickerDetailsPage(),
        binding: TickerDetailsBindings(),
      ),
    ],
  ));
}

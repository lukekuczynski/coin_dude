import 'package:hive/hive.dart';

class FavouriteCoinsDB {
  static const _favouritesKey = 'favouritesKey';
  final Box _favouriteCoinsBox;

  FavouriteCoinsDB(this._favouriteCoinsBox);

  List<String> getFavourites() {
    dynamic result = _favouriteCoinsBox.get(_favouritesKey);
    String stringResult = result is String ? result : '';
    return stringResult.split(',');
  }

  void addToFavourites(String favouriteKey) {
    var favourites = getFavourites();
    if (!favourites.contains(favouriteKey)) {
      favourites.add(favouriteKey);
      _favouriteCoinsBox.put(_favouritesKey, favourites.join(','));
    }
  }

  void removeFromFavourites(String favouriteKey) {
    var favourites = getFavourites();
    favourites.removeWhere((element) => element == favouriteKey);
    _favouriteCoinsBox.put(_favouritesKey, favourites.join(','));
  }
}

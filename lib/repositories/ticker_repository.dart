import 'package:coin_dude/api/coin_paprika_api.dart';
import 'package:coin_dude/db/favourite_coins_db.dart';
import 'package:coin_dude/models/ticker.dart';

class TickerRepository {
  final CoinPaprikaApi _api;
  final FavouriteCoinsDB _db;

  final List<String> _availableConins = [
    'BTC',
    'ETH',
    'ADA',
    'BNB',
    'XRP',
    'SOL',
    'DOGE',
    'DOT',
    'UNI',
    'LINK',
    'LUNA',
    'BCH',
    'LTC',
    'VET'
  ];

  TickerRepository(
    this._api,
    this._db,
  );

  Future<List<Ticker>> getTickers() async {
    var tickers = await _api.getTickers();
    tickers
        .removeWhere((element) => !_availableConins.contains(element.symbol));
    final favourites = _db.getFavourites();
    for (var ticker in tickers) {
      ticker.isFavourite = favourites.contains(ticker.symbol);
    }
    return tickers;
  }

  void addTickerToFavourites(String symbol) {
    _db.addToFavourites(symbol);
  }

  void removeTickerFromFavourites(String symbol) {
    _db.removeFromFavourites(symbol);
  }
}

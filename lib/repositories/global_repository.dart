import 'package:coin_dude/api/coin_paprika_api.dart';
import 'package:coin_dude/models/global_state.dart';

class GlobalRepository {
  final CoinPaprikaApi _api;

  GlobalRepository(
    this._api,
  );

  Future<GlobalState> getGlobalState() async {
    return _api.getGlobalState();
  }
}

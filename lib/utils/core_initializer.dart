import 'package:coin_dude/api/coin_paprika_api.dart';
import 'package:coin_dude/db/favourite_coins_db.dart';
import 'package:coin_dude/modules/coins/coins_controller.dart';
import 'package:coin_dude/repositories/global_repository.dart';
import 'package:coin_dude/repositories/ticker_repository.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

class CoreInitializer {
  static Future initCore() async {
    var box = await Hive.openBox("favourites");
    Get.put(Dio(), permanent: true);
    Get.put(CoinPaprikaApi(Get.find()), permanent: true);
    Get.put(GlobalRepository(Get.find()), permanent: true);
    Get.put(FavouriteCoinsDB(box), permanent: true);
    Get.put(TickerRepository(Get.find(), Get.find()), permanent: true);
  }
}

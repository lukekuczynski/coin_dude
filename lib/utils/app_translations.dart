import 'package:get/get.dart';

class AppTranslations extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en': {
          'app title': 'Coin Dude',
          'home': 'Home',
          'settings': 'Settings',
          'global.title': 'Gloabl Crypto Status',
          'global.marketCapUsd': 'Market Cap:',
          'global.volume24hUsd': 'Market Volume 24H:',
          'global.marketBitcoinDominancePercentageUsd': 'Bitcoin Dominance:',
          'global.cryptocurrenciesNumber': 'Cryptocurrencies',
          'coins': 'Coins',
        }
      };
}

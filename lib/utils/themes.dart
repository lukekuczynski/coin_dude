import 'package:flutter/material.dart';

class Themes {
  static final ligntTheme = ThemeData(
    backgroundColor: Colors.grey[800],
    appBarTheme: AppBarTheme(color: AppColors.brand),
    tabBarTheme: TabBarTheme(
        labelColor: AppColors.brand, unselectedLabelColor: Colors.green),
    buttonTheme: ButtonThemeData(buttonColor: AppColors.brand),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(primary: AppColors.brand)),
    textTheme: TextTheme(
      button: TextStyle(color: AppColors.brand),
    ),
  );
  static final darkTheme = ThemeData();
}

class AppColors {
  static final brand = Color.fromARGB(255, 70, 70, 225);
}

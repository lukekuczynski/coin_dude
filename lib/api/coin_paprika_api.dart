import 'package:coin_dude/models/global_state.dart';
import 'package:coin_dude/models/ticker.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'coin_paprika_api.g.dart';

@RestApi(baseUrl: 'https://api.coinpaprika.com/v1/')
abstract class CoinPaprikaApi {
  factory CoinPaprikaApi(Dio dio, {String baseUrl}) = _CoinPaprikaApi;

  @GET('/global')
  Future<GlobalState> getGlobalState();

  @GET('/tickers?quotes=BTC,USD')
  Future<List<Ticker>> getTickers();
}

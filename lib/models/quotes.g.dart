// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quotes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Quotes _$QuotesFromJson(Map<String, dynamic> json) {
  return Quotes(
    Quote.fromJson(json['BTC'] as Map<String, dynamic>),
    Quote.fromJson(json['USD'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$QuotesToJson(Quotes instance) => <String, dynamic>{
      'BTC': instance.btc,
      'USD': instance.usd,
    };

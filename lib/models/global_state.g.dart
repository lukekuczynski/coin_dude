// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'global_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GlobalState _$GlobalStateFromJson(Map<String, dynamic> json) {
  return GlobalState(
    (json['market_cap_usd'] as num).toDouble(),
    (json['volume_24h_usd'] as num).toDouble(),
    (json['bitcoin_dominance_percentage'] as num).toDouble(),
    json['cryptocurrencies_number'] as int,
    (json['market_cap_ath_value'] as num).toDouble(),
    DateTime.parse(json['market_cap_ath_date'] as String),
    (json['volume_24h_ath_value'] as num).toDouble(),
    DateTime.parse(json['volume_24h_ath_date'] as String),
    (json['market_cap_change_24h'] as num).toDouble(),
    (json['volume_24h_change_24h'] as num).toDouble(),
    json['last_updated'] as int,
  );
}

Map<String, dynamic> _$GlobalStateToJson(GlobalState instance) =>
    <String, dynamic>{
      'market_cap_usd': instance.marketCapUsd,
      'volume_24h_usd': instance.volume24hUsd,
      'bitcoin_dominance_percentage':
          instance.marketBitcoinDominancePercentageUsd,
      'cryptocurrencies_number': instance.cryptocurrenciesNumber,
      'market_cap_ath_value': instance.marketCapAthValue,
      'market_cap_ath_date': instance.marketCapAthDate.toIso8601String(),
      'volume_24h_ath_value': instance.volume24hAthValue,
      'volume_24h_ath_date': instance.volume24hAthDate.toIso8601String(),
      'market_cap_change_24h': instance.marketCapChange24h,
      'volume_24h_change_24h': instance.volume24hChange24h,
      'last_updated': instance.lastUpdated,
    };

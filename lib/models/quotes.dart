import 'package:coin_dude/models/quote.dart';
import 'package:json_annotation/json_annotation.dart';

part 'quotes.g.dart';

@JsonSerializable()
class Quotes {
  @JsonKey(name: 'BTC')
  final Quote btc;

  @JsonKey(name: 'USD')
  final Quote usd;

  Quotes(this.btc, this.usd);

  Map<String, dynamic> toJson() => _$QuotesToJson(this);
  factory Quotes.fromJson(Map<String, dynamic> json) => _$QuotesFromJson(json);
}

import 'package:coin_dude/models/quotes.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ticker.g.dart';

@JsonSerializable()
class Ticker {
  final String id;
  final String name;
  final String symbol;
  final Quotes quotes;

  @JsonKey(ignore: true)
  var isFavourite = false;

  Ticker(this.id, this.name, this.symbol, this.quotes);

  Map<String, dynamic> toJson() => _$TickerToJson(this);
  factory Ticker.fromJson(Map<String, dynamic> json) => _$TickerFromJson(json);
}

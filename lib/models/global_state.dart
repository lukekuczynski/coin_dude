import 'package:json_annotation/json_annotation.dart';

part 'global_state.g.dart';

@JsonSerializable()
class GlobalState {
  @JsonKey(name: 'market_cap_usd')
  final double marketCapUsd;

  @JsonKey(name: 'volume_24h_usd')
  final double volume24hUsd;

  @JsonKey(name: 'bitcoin_dominance_percentage')
  final double marketBitcoinDominancePercentageUsd;

  @JsonKey(name: 'cryptocurrencies_number')
  final int cryptocurrenciesNumber;

  @JsonKey(name: 'market_cap_ath_value')
  final double marketCapAthValue;

  @JsonKey(name: 'market_cap_ath_date')
  final DateTime marketCapAthDate;

  @JsonKey(name: 'volume_24h_ath_value')
  final double volume24hAthValue;

  @JsonKey(name: 'volume_24h_ath_date')
  final DateTime volume24hAthDate;

  @JsonKey(name: 'market_cap_change_24h')
  final double marketCapChange24h;

  @JsonKey(name: 'volume_24h_change_24h')
  final double volume24hChange24h;

  @JsonKey(name: 'last_updated')
  final int lastUpdated;

  GlobalState(
    this.marketCapUsd,
    this.volume24hUsd,
    this.marketBitcoinDominancePercentageUsd,
    this.cryptocurrenciesNumber,
    this.marketCapAthValue,
    this.marketCapAthDate,
    this.volume24hAthValue,
    this.volume24hAthDate,
    this.marketCapChange24h,
    this.volume24hChange24h,
    this.lastUpdated,
  );

  Map<String, dynamic> toJson() => _$GlobalStateToJson(this);
  factory GlobalState.fromJson(Map<String, dynamic> json) =>
      _$GlobalStateFromJson(json);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quote.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Quote _$QuoteFromJson(Map<String, dynamic> json) {
  return Quote(
    (json['price'] as num?)?.toDouble(),
    (json['volume_24h'] as num?)?.toDouble(),
    (json['percent_change_24h'] as num?)?.toDouble(),
  );
}

Map<String, dynamic> _$QuoteToJson(Quote instance) => <String, dynamic>{
      'price': instance.price,
      'volume_24h': instance.volume24h,
      'percent_change_24h': instance.percentChange24h,
    };

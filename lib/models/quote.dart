import 'package:json_annotation/json_annotation.dart';

part 'quote.g.dart';

@JsonSerializable()
class Quote {
  final double? price;

  @JsonKey(name: 'volume_24h')
  final double? volume24h;

  @JsonKey(name: 'percent_change_24h')
  final double? percentChange24h;

  Quote(this.price, this.volume24h, this.percentChange24h);

  Map<String, dynamic> toJson() => _$QuoteToJson(this);
  factory Quote.fromJson(Map<String, dynamic> json) => _$QuoteFromJson(json);
}

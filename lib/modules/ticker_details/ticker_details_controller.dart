import 'package:coin_dude/models/ticker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TickerDetailsController extends GetxController {
  final _ticker = Rx<Ticker?>(null);
  Ticker? get ticker => _ticker.value;
  final emailController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    _ticker.value = Get.arguments['ticker'];
  }

  @override
  void onClose() {
    super.onClose();
  }

  void subscribeOnTap() {
    if (emailController.text.isEmail) {
      returnToPrevious(emailController.text);
      return;
    }

    Get.defaultDialog(
      title: 'Not valid email!',
      actions: <Widget>[
        new TextButton(
          onPressed: () {
            Get.back();
          },
          child: Text('OK'),
        )
      ],
    );
  }

  void returnToPrevious(String email) {
    Get.back(result: email);
  }
}

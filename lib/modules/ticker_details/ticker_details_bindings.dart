import 'package:coin_dude/modules/ticker_details/ticker_details_controller.dart';
import 'package:get/get.dart';

class TickerDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TickerDetailsController>(() => TickerDetailsController());
  }
}

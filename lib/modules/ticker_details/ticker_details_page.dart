import 'package:coin_dude/modules/ticker_details/ticker_details_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TickerDetailsPage extends GetView<TickerDetailsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Coin details'),
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 20),
            Text(
              controller.ticker?.name ?? '',
              style: TextStyle(fontSize: 24),
            ),
            SizedBox(height: 30),
            Text('subscribe to our newsletter:'),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                controller: controller.emailController,
                decoration: InputDecoration(labelText: 'email'),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            ElevatedButton(
                onPressed: () => controller.subscribeOnTap(),
                child: Text('Subscribe')),
          ],
        ),
      ),
    );
  }
}

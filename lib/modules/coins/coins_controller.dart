import 'package:coin_dude/models/ticker.dart';
import 'package:coin_dude/repositories/ticker_repository.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class CoinsController extends GetxController {
  final TickerRepository _tickerRepository;
  final _tickers = Rx<List<Ticker>>([]);
  List<Ticker> get tickers => _tickers.value;
  List<Ticker> get favouriteTickers =>
      _tickers.value.where((ticker) => ticker.isFavourite).toList();

  CoinsController(this._tickerRepository);

  @override
  void onInit() {
    super.onInit();

    _tickerRepository.getTickers().catchError((error) {
      print(error);
    }).then((value) {
      _tickers.value = value;
    });
  }

  void favouriteOnTap(String symbol, bool isFavourite) {
    if (isFavourite) {
      _tickerRepository.removeTickerFromFavourites(symbol);
    } else {
      _tickerRepository.addTickerToFavourites(symbol);
    }

    _tickers.refresh();
  }

  Future<void> tickerOnTap(Ticker ticker) async {
    final result =
        await Get.toNamed("/ticker_details", arguments: {'ticker': ticker});
    if (result is String) {
      Get.snackbar(
        'Subscription email',
        result,
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }
}

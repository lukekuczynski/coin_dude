import 'package:coin_dude/modules/coins/coins_controller.dart';
import 'package:coin_dude/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class CoinsPage extends GetView<CoinsController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Row(
        children: [
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              // child: SingleChildScrollView(
              // scrollDirection: Axis.vertical,
              child: DataTable(
                  columnSpacing: 8,
                  showCheckboxColumn: false,
                  columns: const [
                    DataColumn(label: Text('Coin')),
                    DataColumn(label: Text('USD Quote')),
                    DataColumn(label: Text('% 24h')),
                    DataColumn(label: Text('Fav')),
                  ],
                  rows: controller.tickers
                      .map((ticker) => DataRow(
                            onSelectChanged: (isSelected) {
                              if (isSelected == false) {
                                return;
                              }

                              controller.tickerOnTap(ticker);
                            },
                            cells: <DataCell>[
                              DataCell(Text('${ticker.name}')),
                              DataCell(Text(
                                  '\$${ticker.quotes.usd.price?.toStringAsFixed(2)}')),
                              DataCell(Text(
                                '${ticker.quotes.usd.percentChange24h?.toStringAsPrecision(2)}%',
                                style: TextStyle(
                                    color:
                                        (ticker.quotes.usd.percentChange24h ??
                                                    0) >
                                                0
                                            ? Colors.green
                                            : Colors.red),
                              )),
                              DataCell(IconButton(
                                icon: Icon(
                                  ticker.isFavourite
                                      ? Icons.star
                                      : Icons.star_border,
                                  color: AppColors.brand,
                                ),
                                onPressed: () {
                                  controller.favouriteOnTap(
                                      ticker.symbol, ticker.isFavourite);
                                  ticker.isFavourite = !ticker.isFavourite;
                                },
                              )),
                            ],
                          ))
                      .toList()),
            ),
          ),
        ],
      ),
      // ),
    ); //GridView(gridDelegate: gridDelegate)
  }
}

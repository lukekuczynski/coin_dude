import 'package:coin_dude/models/global_state.dart';
import 'package:coin_dude/repositories/global_repository.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:intl/intl.dart';

class GlobalController extends GetxController {
  final GlobalRepository _globalRepository;
  final _globalState = Rx<GlobalState?>(null);
  final _isLoading = Rx<bool>(false);
  final dollarFormatter = NumberFormat.currency(
      locale: Get.locale?.scriptCode ?? 'en-US', symbol: '＄');
  final formatter = NumberFormat(',###.##');

  bool get isLoading => _isLoading.value;
  bool get isGlobalStateAvailable => _globalState.value != null;

  String get marketCapUsd {
    return dollarFormatter.format(_globalState.value?.marketCapUsd ?? 0);
  }

  String get volume24hUsd {
    return dollarFormatter.format(_globalState.value?.volume24hUsd ?? 0);
  }

  String get marketBitcoinDominancePercentageUsd {
    final formatterPercentage = formatter
        .format(_globalState.value?.marketBitcoinDominancePercentageUsd ?? 0);
    return '$formatterPercentage%';
  }

  String get cryptocurrenciesNumber {
    return formatter.format(_globalState.value?.cryptocurrenciesNumber ?? 0);
  }

  GlobalController(this._globalRepository);

  @override
  void onInit() {
    super.onInit();

    _isLoading.value = true;
    _globalRepository.getGlobalState().then((value) {
      _globalState.value = value;
      _isLoading.value = false;
    }).catchError((error) {
      print(error);
      _isLoading.value = false;
    });
  }
}

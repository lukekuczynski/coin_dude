import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'global_controller.dart';

class GlobalView extends GetView<GlobalController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: controller.isLoading
                  ? Center(
                      child: SizedBox(
                        child: CircularProgressIndicator(),
                        width: 32,
                        height: 32,
                      ),
                    )
                  : Column(
                      children: [
                        Text(
                          'global.title'.tr,
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        gloabDataRow(
                          title: 'global.marketCapUsd'.tr,
                          value: controller.marketCapUsd,
                        ),
                        gloabDataRow(
                          title: 'global.volume24hUsd'.tr,
                          value: controller.volume24hUsd,
                        ),
                        gloabDataRow(
                          title:
                              'global.marketBitcoinDominancePercentageUsd'.tr,
                          value: controller.marketBitcoinDominancePercentageUsd,
                        ),
                        gloabDataRow(
                          title: 'global.cryptocurrenciesNumber'.tr,
                          value: controller.cryptocurrenciesNumber,
                        ),
                      ],
                    ),
            ),
          ),
        ));
  }

  Row gloabDataRow({required String title, required String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Text(value),
      ],
    );
  }
}

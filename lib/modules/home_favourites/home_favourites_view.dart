import 'package:carousel_slider/carousel_slider.dart';
import 'package:coin_dude/modules/coins/coins_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeFavouritesView extends GetView<CoinsController> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Text(
                'Your favourites',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(height: 8),
              Obx(
                () => CarouselSlider(
                  items: controller.favouriteTickers
                      .map((ticker) => Container(
                            child: Row(
                              children: [
                                Text(
                                  ticker.symbol,
                                  style: TextStyle(fontSize: 20),
                                ),
                                SizedBox(width: 8.0),
                                Text(
                                  '\$${ticker.quotes.usd.price?.toStringAsFixed(2)}',
                                  style: TextStyle(fontSize: 20),
                                ),
                                SizedBox(width: 8.0),
                                Text(
                                  '${ticker.quotes.usd.percentChange24h?.toStringAsFixed(2)}%',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color:
                                          (ticker.quotes.usd.percentChange24h ??
                                                      0) >
                                                  0
                                              ? Colors.green
                                              : Colors.red),
                                ),
                              ],
                            ),
                          ))
                      .toList(),
                  options: CarouselOptions(
                    height: 20.0,
                    autoPlay: true,
                    autoPlayInterval: Duration(seconds: 2),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:get/get.dart';
import 'package:hive/hive.dart';

class AppController extends GetxController {
  final _currentSelectedTabIndex = 0.obs;
  int get currentSelectedTabIndex => _currentSelectedTabIndex.value;

  void bottomNavigationBarOnTap(int index) {
    _currentSelectedTabIndex.value = index;
  }

  @override
  void dispose() {
    Hive.close();
    super.dispose();
  }
}

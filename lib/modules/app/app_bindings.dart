import 'package:coin_dude/api/coin_paprika_api.dart';
import 'package:coin_dude/db/favourite_coins_db.dart';
import 'package:coin_dude/modules/app/app_controller.dart';
import 'package:coin_dude/modules/coins/coins_controller.dart';
import 'package:coin_dude/modules/global/global_controller.dart';
import 'package:coin_dude/repositories/global_repository.dart';
import 'package:coin_dude/repositories/ticker_repository.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

class AppBindings extends Bindings {
  @override
  void dependencies() async {
    Get.lazyPut<AppController>(() => AppController());
    Get.lazyPut<GlobalController>(() => GlobalController(Get.find()));
    Get.lazyPut(() => CoinsController(Get.find()));
  }
}

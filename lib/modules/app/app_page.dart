// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

import 'package:coin_dude/modules/coins/coins_page.dart';
import 'package:coin_dude/modules/home/home_page.dart';
import 'package:coin_dude/utils/themes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'app_controller.dart';

class AppPage extends GetView<AppController> {
  static List<Widget> _tabs = <Widget>[
    HomePage(),
    CoinsPage(),
    Text('settings'.tr),
  ];

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        appBar: AppBar(title: Text('app title'.tr)),
        body: _tabs.elementAt(controller.currentSelectedTabIndex),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: AppColors.brand,
          items: [
            BottomNavigationBarItem(
              activeIcon: Icon(
                Icons.home_filled,
                color: AppColors.brand,
              ),
              icon: Icon(
                Icons.home_filled,
                color: Colors.grey,
              ),
              label: 'home'.tr,
            ),
            BottomNavigationBarItem(
              activeIcon: Icon(
                Icons.toc,
                color: AppColors.brand,
              ),
              icon: Icon(
                Icons.toc,
                color: Colors.grey,
              ),
              label: 'coins'.tr,
            ),
            BottomNavigationBarItem(
              activeIcon: Icon(
                Icons.settings,
                color: AppColors.brand,
              ),
              icon: Icon(
                Icons.settings,
                color: Colors.grey,
              ),
              label: 'settings'.tr,
            ),
          ],
          currentIndex: controller.currentSelectedTabIndex,
          onTap: controller.bottomNavigationBarOnTap,
        ),
      ),
    );
  }
}

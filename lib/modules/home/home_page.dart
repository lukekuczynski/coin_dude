import 'package:coin_dude/modules/global/global_view.dart';
import 'package:coin_dude/modules/home_favourites/home_favourites_view.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: ListView(
        children: [
          GlobalView(),
          HomeFavouritesView(),
        ],
      ),
    );
  }
}
